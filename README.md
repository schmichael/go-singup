Signup API
==========

See https://gitlab.com/pietvanzoen/signup

Installation
------------

1. Install Go:

Read [Go's documentation](http://golang.org/doc/install) or use a system specific method:

**OSX with Brew**

```
brew install go
```

**Ubuntu or Debian**

```
sudo apt-get install golang
```

2. Set `$GOPATH`

On Linux, OSX, or other UNIXes, create a directory to hold Go files and
binaries. This will be your `$GOPATH`.

```
mkdir ~/go
export GOPATH=$HOME/go  # Place this line in your ~/.bashrc or ~/.bash_profile
```

3. go install

Finally install go-signup:

```
go get -u gitlab.com/schmichael/go-signup
```

Run the command at any time to get the latest version.

4. Starting the API

```
cd $GOPATH/bin
./go-signup
```

You can stop the API by pressing `CTRL-C`.


Test data
---------

Some handy SQL statements to insert stuff once the database is created:

Open the database with:

```shell
sqlite3 signup.sqlite3
```

Then run:

```sql
INSERT INTO schedules (id, name, description) VALUES ("abcd", "sched 1", "this is a sched");

INSERT INTO schedule_dates (id, schedule, date, description, need) VALUES ("efg", "abcd", "2014-05-08 11:11:11", "this is a schedule date", 9);
```
