package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

type Schedule struct {
	ID          string          `json:"id"`
	Name        string          `json:"name"`
	Description string          `json:"description"`
	Dates       []*ScheduleDate `json:"dates"`
}

type ScheduleDate struct {
	ID          string    `json:"id"`
	Date        time.Time `json:"date"`
	Description string    `json:"description"`
	Need        int       `json:"need"`
}

var DB *sql.DB

func SchedulesHandler(rw http.ResponseWriter, req *http.Request) {
	tx, err := DB.Begin()
	if err != nil {
		log.Printf("Error begining transaction: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	defer tx.Rollback()

	schedCursor, err := tx.Query("SELECT * FROM schedules")
	if err != nil {
		log.Printf("Error retrieving schedules: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}

	var scheds []*Schedule

	for schedCursor.Next() {
		sched := &Schedule{Dates: make([]*ScheduleDate, 0)}
		if err := schedCursor.Scan(&sched.ID, &sched.Name, &sched.Description); err != nil {
			log.Printf("Error scanning schedule: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		}
		datesCursor, err := tx.Query("SELECT id, date, description, need FROM schedule_dates WHERE schedule = ?", sched.ID)
		if err != nil {
			log.Printf("Error retrieving schedule dates: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		}
		for datesCursor.Next() {
			d := &ScheduleDate{}
			date := ""
			if err := datesCursor.Scan(&d.ID, &date, &d.Description, &d.Need); err != nil {
				log.Printf("Error scanning schedule date: %v", err)
				rw.WriteHeader(500)
				rw.Write([]byte("oops"))
				return
			}
			if d.Date, err = time.Parse("2006-01-02 15:04:05", date); err != nil {
				log.Printf("Error parsing date: %v", err)
				rw.WriteHeader(500)
				rw.Write([]byte("oops"))
				return
			}
			sched.Dates = append(sched.Dates, d)
		}
		scheds = append(scheds, sched)
	}

	resp := &struct {
		Schedules []*Schedule `json:"schedules"`
	}{Schedules: scheds}
	body, err := json.Marshal(resp)
	if err != nil {
		log.Printf("Error encoding schedules: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	rw.WriteHeader(200)
	rw.Write(body)
}

func createTable(table, create string) {
	create = fmt.Sprintf(create, table)

	tx, err := DB.Begin()
	if err != nil {
		log.Fatalf("Error beginning transaction: %v", err)
	}

	row := tx.QueryRow("SELECT COUNT(1) FROM sqlite_master WHERE type = ? AND name = ?", "table", table)
	count := 0
	if err := row.Scan(&count); err != nil {
		log.Fatalf("Error reading results of '%s' table check: %v", table, err)
	}

	if count > 0 {
		tx.Rollback()
		return
	}
	if _, err := tx.Exec(create); err != nil {
		log.Fatalf("Error creating table %s: %v", table, err)
	}
	if err := tx.Commit(); err != nil {
		log.Fatalf("Error commiting table creation %s: %v", table, err)
	}
}

func initDB() {
	createTable("schedules", "CREATE TABLE %s ( id TEXT PRIMARY KEY, name TEXT, description TEXT )")

	createTable("schedule_dates", "CREATE TABLE %s ( id TEXT PRIMARY KEY, schedule TEXT, date TEXT, need INTEGER, description TEXT, FOREIGN KEY(schedule) REFERENCES schedules(id) )")

	createTable("schedule_signups", "CREATE TABLE %s ( id TEXT PRIMARY KEY, schedule_date TEXT, person TEXT, FOREIGN KEY(schedule_date) REFERENCES schedule_dates(id) )")
}

func main() {
	bind := flag.String("bind", "localhost:8000", "host:port to listen on")
	dbFile := flag.String("db", "signup.sqlite3", "filename for db")
	flag.Parse()

	var err error
	if DB, err = sql.Open("sqlite3", *dbFile); err != nil {
		log.Fatalf("Error opening database: %v", err)
	}

	initDB()

	r := mux.NewRouter()
	r.HandleFunc("/api/schedules", SchedulesHandler).Methods("GET")

	log.Printf("Starting %s on http://%s", path.Base(os.Args[0]), *bind)
	log.Fatal(http.ListenAndServe(*bind, r))
}
